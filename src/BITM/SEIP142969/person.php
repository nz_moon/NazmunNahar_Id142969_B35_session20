<?php
namespace App;


class Person
{
    public $name="Nazmun";
    public $gender="Female";
    public $blood_group="A+";

    public function showPersonInfo(){

        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";
    }

}